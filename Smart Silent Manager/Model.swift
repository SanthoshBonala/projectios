//
//  Model.swift
//  Smart Silent Manager
//
//  Created by Darshan Reddy on 3/9/18.
//  Copyright © 2018 student. All rights reserved.
//

import Foundation

struct Event {
    private var _eventName:String
    private var _description:String
    private var _startTime:DateComponents
    private var _endTime:DateComponents
    private var _repeatDays:[NSCalendar.Unit]
    
    var eventName:String { return _eventName }
    var description:String { return _description }
    var startTime:DateComponents { return _startTime }
    var endTime:DateComponents { return _endTime }
    var repeatDays:[Int] { get {
        var days:[Int] = []
        for i in _repeatDays {
            days.append(Int(i.rawValue))
        }
        return days
        }
    }
    
    init(eventName:String, description:String, startTime:DateComponents, endTime:DateComponents, repeatDays:[Int]){
        _eventName = eventName
        _description = description
        _startTime = startTime
        _endTime = endTime
        _repeatDays = []
        for i in repeatDays {
            var day = NSCalendar.Unit.day
            day.insert(NSCalendar.Unit.init(rawValue: UInt(i)))
            _repeatDays.append(day)
        }
    }
}


class EventManager {
    private var _events:[Event] = []
    
    var repeatDays : [[Int]] = [[],[],[],[]]
    
    var index:Int?
    
    var events:[Event] { return _events }
    
    init(){
        loadData()
    }
    
    func loadData(){
        var startDate = DateComponents()
        startDate.hour = 9
        startDate.minute = 35
        var endDate = DateComponents()
        endDate.hour = 10
        endDate.minute = 50
        _events.append(Event(eventName: "IOS Class", description: "IOS regular class", startTime: startDate, endTime: endDate, repeatDays: [1,3]))
        
        var startDate1 = DateComponents()
        startDate1.hour = 12
        startDate1.minute = 00
        var endDate1 = DateComponents()
        endDate1.hour = 13
        endDate1.minute = 50
        _events.append(Event(eventName: "NS Class", description: "NS lab", startTime: startDate1, endTime: endDate1, repeatDays: [0,2,4]))
        
        var startDate2 = DateComponents()
        startDate2.hour = 5
        startDate2.minute = 30
        var endDate2 = DateComponents()
        endDate2.hour = 7
        endDate2.minute = 30
        _events.append(Event(eventName: "ADB Class", description: "ADB help class", startTime: startDate2, endTime: endDate2, repeatDays: [0,2,4]))
    }
    
    func addEvent(eventName:String, description:String, startTime:DateComponents, endTime:DateComponents, repeatDays:[Int]){
        _events.append(Event(eventName: eventName, description: description, startTime: startTime, endTime: endTime, repeatDays: repeatDays))
        
    }
    
    func editEvent(eventIndex:Int, eventName:String, description:String, startTime:DateComponents, endTime:DateComponents, repeatDays:[Int]){
        _events.remove(at: eventIndex)
        _events.insert(Event(eventName: eventName, description: description, startTime: startTime, endTime: endTime, repeatDays: repeatDays), at: eventIndex)
    }
    
    func deleteEvent(eventIndex:Int){
        _events.remove(at: eventIndex)
    }
}

