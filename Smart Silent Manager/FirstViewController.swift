//
//  FirstViewController.swift
//  Smart Silent Manager
//
//  Created by student on 2/16/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var data = AppDelegate.events

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
        cell.textLabel?.text = data.events[indexPath.row].eventName
        let event = data.events[indexPath.row]
        cell.detailTextLabel?.text = "\(event.startTime.hour!) : \(event.startTime.minute!)"
        
        //cell.backgroundView?.tintColor = UIColor.darkGray
        return cell
    }
    
    
    @IBAction func goToAdd(_ sender: Any) {
       self.tabBarController!.selectedIndex = 1
        
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

