//
//  EventTableViewCell.swift
//  Smart Silent Manager
//
//  Created by student on 3/9/18.
//  Copyright © 2018 student. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func editBTNClicked(_ sender: Any) {
        
        let s = sender as? UIButton
        AppDelegate.events.index = s!.tag
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var eventName: UILabel!
    
    @IBOutlet weak var eventTime: UILabel!
    
    @IBOutlet weak var eventDay: UILabel!
    
    @IBOutlet weak var editBTN: UIButton!
    
    
    
    
    
    
    
    

}
